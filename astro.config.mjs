import { defineConfig } from 'astro/config';
import tailwind from '@astrojs/tailwind';
import sitemap from '@astrojs/sitemap';
import mdx from '@astrojs/mdx';

const i18n = {
  defaultLocale: 'en',
  locales: {
    en: 'en-US',
    es: 'es-ES',
    no: 'nb-NO',
  },
};

// https://astro.build/config
export default defineConfig({
  site: 'https://www.stevenreina.com',
  prefetch: {
    prefetchAll: false,
  },
  i18n: {
    ...i18n,
    locales: Object.keys(i18n.locales),
  },
  integrations: [
    tailwind(),
    mdx(),
    sitemap({ i18n }),
  ],
});
