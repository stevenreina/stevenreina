// @ts-check
import astro from 'eslint-plugin-astro';
import tailwindcss from 'eslint-plugin-tailwindcss';

import eslint from '@eslint/js';
import tseslint from 'typescript-eslint';

export default tseslint.config(
  {
    ignores: [
      'dist/**/*',
      'node_modules/**/*',
      '**/.*'
    ],
  },
  eslint.configs.recommended,
  tseslint.configs.recommended,
  ...tailwindcss.configs['flat/recommended'],
  ...astro.configs['flat/recommended'],
  {
    rules: {
      indent: ['error', 2],
      'linebreak-style': ['error', 'unix'],
      quotes: ['error', 'single'],
      semi: ['error', 'always'],
    },
  },
  {
    files: ['netlify/**/*.js'],
    rules: {
      '@typescript-eslint/no-var-requires': 0,
    },
  },
);
