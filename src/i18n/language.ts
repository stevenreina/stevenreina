export type Language = {
  name: string,
  short: string,
  flag: number[]
}

export const langs = {
  en: {
    name: 'English',
    short: 'en',
    flag: [0x1F1EC, 0x1F1E7]
  } as Language,
  es: {
    name: 'Español',
    short: 'es',
    flag: [0x1F1EA, 0x1F1F8]
  } as Language,
  no: {
    name: 'Norsk',
    short: 'no',
    flag: [0x1F1F3, 0x1F1F4]
  } as Language,
} as const;
