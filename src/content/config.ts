import i18n from './_definitions/i18n';
import projectData from './_definitions/projectData';

export const collections = {
  ...i18n,
  projectData
};
