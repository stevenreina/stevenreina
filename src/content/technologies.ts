export class Technology {
  name: string;
  logo: string;
  webpage: string;

  constructor (name: string, logo: string, webpage: string) {
    this.name = name;
    this.logo = logo;
    this.webpage = webpage;
  }
}

export default {
  stdStack: new Technology('HTML, CSS & JS', 'html5_css3_javascript5.png', 'https://developer.mozilla.org/en-US/'),
  astro: new Technology('Astro', 'astro.png', 'https://astro.build'),
  tailwind: new Technology('Tailwind', 'tailwind-css.png', 'https://tailwindcss.com'),
  typescript: new Technology('Typescript', 'Typescript_logo_2020.svg.png', 'https://www.typescriptlang.org'),
  parcel: new Technology('Parcel', 'parceljs-logo.png', 'https://parceljs.org'),
  vite: new Technology('Vite', '65625612.png', 'https://vitejs.dev'),
  netlify: new Technology('Netlify', 'netlify-icon-256x256-psuiw2x7.png', 'https://www.netlify.com'),
  gitlabCi: new Technology('GitLab CI', 'gitlab-logo.png', 'https://gitlab.com'),
  stylelint: new Technology('Stylelint', 'image_processing20210620-25158-16fib77.png', 'https://stylelint.io/'),
  eslint: new Technology('ESLint', 'eslint-logo.png', 'https://eslint.org/'),
  alpine: new Technology('Alpine.js', 'alpinejs-logo.png', 'https://alpinejs.dev/'),
  lit: new Technology('Lit', 'lit-logo.png', 'https://lit.dev/'),
  solidjs: new Technology('SolidJS', 'solidjs-logo.png', 'https://solidjs.com/'),
};
