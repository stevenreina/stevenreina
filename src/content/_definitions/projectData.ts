import { defineCollection, z } from 'astro:content';

import type techs from '../technologies';

export default defineCollection({
  type: 'data',
  schema: z.object({
    thumbnailPath: z.string(),
    projectUrl: z.string(),
    technologies: z.array(
      z.custom<keyof typeof techs>()
    )
  })
});
