import { defineCollection, z } from 'astro:content';

export default {
  description_i18n: defineCollection({ type: 'content' }),
  sections_i18n: defineCollection({
    type: 'data',
    schema: z.object({
      home: z.string(),
      projects: z.string(),
      contact: z.string(),
    }),
  }),
  footer_i18n: defineCollection({
    type: 'data',
    schema: z.object({
      linkedinTitle: z.string(),
      gitlabTitle: z.string(),
    }),
  }),
  home_i18n: defineCollection({
    type: 'content',
    schema: z.object({
      figcaption_0: z.string(),
    }),
  }),
  projects_i18n: defineCollection({
    type: 'data',
    schema: z.object({
      technologiesLabel: z.string(),
      projectsArray: z.array(
        z.object({
          name: z.string(),
          description: z.string(),
        }),
      ),
    })
  }),
  contact_i18n: defineCollection({
    type: 'data',
    schema: z.object({
      name: z.string(),
      email: z.string(),
      reason: z.string(),
      message: z.string(),
      send: z.string(),
      toast_success_title: z.string(),
      toast_success_description: z.string(),
      toast_failure_empty_body_description: z.string(),
      toast_failure_field_length_description: z.string(),
      toast_failure_email_format_description: z.string(),
      toast_failure_unknown_description: z.string(),
    }),
  }),
  '404_i18n': defineCollection({ type: 'content' }),
};
