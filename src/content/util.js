// Based on @bsiddiqui/get-age (https://github.com/bsiddiqui/get-age)
export function getMyAge () {
  const MY_BIRTH_DATE = '1998-11-04';
  const today = new Date();
  const birthDate = new Date(MY_BIRTH_DATE);
  let age = today.getUTCFullYear() - birthDate.getUTCFullYear();
  const month = today.getUTCMonth() - birthDate.getUTCMonth();
  if (month < 0 || (month === 0 && today.getUTCDate() < birthDate.getUTCDate())) {
    age--;
  }
  return age;
}
