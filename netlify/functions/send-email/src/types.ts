import { z } from 'zod';

export type Range = {
  min: number,
  max: number,
};

export const ContactFormData = z.object({
  name: z.string().nonempty().max(128),
  email: z.string().email().nonempty().max(512),
  reason: z.preprocess((a) => a || undefined, z.string().min(2).max(512).optional()),
  linkedin: z.preprocess((a) => a || undefined, z.string().url().max(512).optional()),
  message: z.string().nonempty().max(8192),
  lang: z.string().nonempty(),
});
export type ContactFormData = z.infer<typeof ContactFormData>;

export type ResponseData = {
  error?: { type: 'ise' | 'empty-body' | 'email-format' | 'unknown' }
    | {
      type: 'field-length',
      field: keyof ContactFormData,
    }
};

export const SuccessHttpStatusCode = z.number().gte(200).lt(300);
export type SuccessHttpStatusCode = z.infer<typeof SuccessHttpStatusCode>;
