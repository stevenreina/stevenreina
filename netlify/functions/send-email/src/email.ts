import { SendSmtpEmail, TransactionalEmailsApi, TransactionalEmailsApiApiKeys } from '@getbrevo/brevo';
import type { ContactFormData } from './types';

const emailApiInstance = new TransactionalEmailsApi();
const apiKey = process.env.BREVO_API_KEY!;
const keyType = TransactionalEmailsApiApiKeys.apiKey;
emailApiInstance.setApiKey(keyType, apiKey);

export const composeEmail = (
  { name, email, linkedin, reason, message }: ContactFormData,
  recipient: string,
) => ({
  ...new SendSmtpEmail(),
  sender: { name, email },
  to: [{ email: recipient }],
  subject: `${name} | from stevenreina.com`,
  htmlContent: `
    <h1>${reason || '<span style="color:#444">No reason</span>'}</h1>
    <p>Name: ${name}</p>
    <p>E-mail: ${email}</p>
    <p>LinkedIn: ${linkedin}</p>
    <br>
    <p>${message}</p>
  `,
});
export const trySendEmail = async (emailMessage: SendSmtpEmail) =>
  await emailApiInstance.sendTransacEmail(emailMessage);
