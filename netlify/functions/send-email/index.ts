import type { Handler, HandlerResponse } from '@netlify/functions';
import { z } from 'zod';

import type { ResponseData } from './src/types';
import { ContactFormData, SuccessHttpStatusCode } from './src/types';
import { composeEmail, trySendEmail } from './src/email';

const makeResponse = (
  statusCode: number,
  error?: ResponseData['error']
): HandlerResponse => {
  const responseData: ResponseData = { error };
  return {
    statusCode,
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(responseData),
  };
};

export const handler: Handler = async (event) => {
  const recipientEmail = process.env.CONTACT_EMAIL;
  if (!z.string().email().safeParse(recipientEmail).success) {
    console.error('CONTACT_EMAIL must be a valid e-mail address');
    return makeResponse(500, { type: 'ise' });
  }
  const requestBody = event.body;
  if (!z.string().nonempty().safeParse(requestBody).success) {
    console.error('Request body is empty');
    return makeResponse(400, { type: 'empty-body' });
  }
  const uncheckedForm = Object.fromEntries(new URLSearchParams(requestBody!));
  let form;
  try {
    form = ContactFormData.parse(uncheckedForm);
  } catch (e) {
    const { issues } = e as z.ZodError;
    const { code: issueCode, path } = issues[0];
    switch (issueCode) {
    case 'too_small':
    case 'too_big':
      return makeResponse(400, {
        type: 'field-length',
        field: path[0] as keyof ContactFormData
      });
    case 'invalid_string':
      if (issues[0].validation === 'email')
        return makeResponse(400, { type: 'email-format' });
    }
    console.error(e);
    return makeResponse(400, { type: 'unknown' });
  }
  try {
    const mailObject = composeEmail(form, recipientEmail!);
    const sendMailResponse = await trySendEmail(mailObject);
    const { complete, statusCode } = sendMailResponse.response;
    if (!complete || !SuccessHttpStatusCode.safeParse(statusCode).success)
      throw new Error('Mail sending didn\'t run successfully');
    return makeResponse(201);
  } catch (e) {
    console.error(e);
    return makeResponse(500, { type: 'ise' });
  }
};
