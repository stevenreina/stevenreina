/** @type {import('tailwindcss').Config} */
export default {
  content: [
    './src/**/*.astro'
  ],
  theme: {
    fontFamily: {
      'default': ['system-ui', 'sans-serif'],
      'Hitmo': ['Hitmo', 'system-ui', 'sans-serif'],
      'MinSans': ['MinSans', 'system-ui', 'sans-serif'],
      'Ampero': ['Ampero', 'system-ui', 'sans-serif'],
      'Garet': ['Garet', 'system-ui', 'sans-serif'],
      'CreatoDisplay': ['CreatoDisplay', 'system-ui', 'sans-serif'],
    },
    extend: {
      backgroundColor: {
        'light': '#F6F6F6',
        'dark': '#222222'
      },
      screens: {
        '3xl': '1750px'
      }
    },
  },
  plugins: [],
};
